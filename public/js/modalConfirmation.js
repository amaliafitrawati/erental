const modal = document.querySelector(".overlay-modal");
const deleteButtons = document.querySelectorAll(".delete-button");
const noButton = document.querySelector(".no-button");
const yesButton = document.querySelector(".yes-button");
let listIndex;
let href;
// console.log(deleteButtons);

deleteButtons.forEach((deleteButton) => {
    deleteButton.addEventListener("click", function () {
        modal.classList.add("active-modal");
        listIndex = deleteButton.getAttribute("data");
        href = yesButton.getAttribute("href");
        if (listIndex !== null) {
            yesButton.setAttribute("href", `${href}${listIndex}`);
        }
        // console.log(yesButton);
        // console.log(listIndex);
    });
});

noButton.addEventListener("click", function () {
    modal.classList.remove("active-modal");
    yesButton.setAttribute("href", `${href}`);
});
