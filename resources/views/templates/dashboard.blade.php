@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
    </head>
<body>
<div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                <div class="card-body py-3">
                    <h3>Hai <?php echo $user ?>!</h3>
                    <h5>Selamat datang di aplikasi eRental</h5>
                </div>
</div>
</div>
</div>
</div>
</div>

</body>
</html>