<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/sidebar.css') }}" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Rental.com</title>
    </head>
<body>
   
    <div class="wrapper">
       <div class="section">
  </div>
        <div class="sidebar">
            <div class="profile">
            <img src="/img/rent-car.png">
                <h3>Rental.com</h3>
            </div>
            <ul>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('dashboard')}}">
                        <i class="fas fa-fw fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php if(Auth::user()->role == 1){ echo '';?>
                <li>
                    <a href="{{route('user')}}">
                        <span class="icon"><i class="fas fa-user-friends"></i></span>
                        <span class="item">User</span>
                    </a>
                </li>
                <li>
                        <a href="{{route('product')}}">
                            <span class="icon"><i class="fas fa-truck"></i></span>
                            <span class="item">List Produk</span>
                        </a>
                    </li>
                <li>
                    <a href="{{route('listProductType')}}">
                        <span class="icon"><i class="fas fa-truck"></i></span>
                        <span class="item">Tipe Produk</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('listTransaction')}}">
                        <span class="icon"><i class="fa fa-fw fa-book"></i></span>
                        <span class="item">List Transaksi</span>
                    </a>
                </li>
                <li>
                    <a href="/report">
                        <span class="icon"><i class="fas fa-chart-line"></i></span>
                        <span class="item">Laporan</span>
                    </a>
                </li>
                <?php }?>
                
                <?php if(Auth::user()->role == 2){ echo '';?>
                <li>
                        <a href="{{route('catalog')}}">
                            <span class="icon"><i class="fas fa-truck"></i></span>
                            <span class="item">Katalog Produk</span>
                        </a>
                    </li>
                <li>
                <li>
                    <a href="{{route('formTransaction')}}">
                        <span class="icon"><i class="fa fa-fw fa-book"></i></span>
                        <span class="item">Form Transaksi</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('listTransactionUser')}}">
                        <span class="icon"><i class="fa fa-fw fa-book"></i></span>
                        <span class="item">Transaksi</span>
                    </a>
                </li>
                <li>
                    <a href="/invoice">
                        <span class="icon"><i class="fas fa-chart-line"></i></span>
                        <span class="item">Invoice</span>
                    </a>
                </li>
                <?php }?>
                <li>
                    <a href="{{route('logout')}}">
                        <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                        <span class="item">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
 
</body>

</html>
