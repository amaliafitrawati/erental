@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tambah Produk</h6>
        </div>
        <div class="card-body">
            <form action="/product/save" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama Kendaraan</label>
                          <input type="text" name="name" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                            <div class="form-group">
                            <label>Jenis Kendaraan</label>
                            <select name="type_id" id="type" class="form-control">
                                <option value="" disabled>Pilih</option>
                                @foreach ($type as $id => $name)
                                    <option value="{{$id}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Plat Nomor</label>
                          <input type="text" name="license_number" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Tahun</label>
                          <input type="text" name="year" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Harga Sewa Perhari</label>
                          <input type="text" name="price" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Foto Produk</label>
                    <input class="form-control" type="file" name="product_image" id="formFile">
                </div>
                <div class="row">
                    <div class="col text-center mt-3">
                        <div class="form-group">
                            <a class="btn btn-danger shadow-sm mr-2" href="{{route('product')}}">Batal</a>
                            <button type="submit" class="btn btn-primary  shadow-sm">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>