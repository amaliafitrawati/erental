@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Produk</h6>
        </div>
        <div class="card-body">
            @foreach($product as $p)
            <form action="/product/update" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $p->id }}">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama Kendaraan</label>
                          <input type="text" name="name" id="" value="{{ $p->name }}" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                            <div class="form-group">
                            <label>Jenis Kendaraan</label>
                            <select name="type_id" id="type" value="{{ $p->type_id }}" class="form-control">
                                <option value="" disabled>Pilih</option>
                                @foreach ($type as $id => $name)
                                    <option value="{{$id}}">{{ $name }}</option>
                                @endforeach
                            </select>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Plat Nomor</label>
                          <input type="text" name="license_number" id=""  value="{{ $p->license_number }}" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Tahun</label>
                          <input type="text" name="year" id="" value="{{ $p->year }}" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Harga Sewa Perhari</label>
                          <input type="text" name="price" id="" value="{{ $p->price }}" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="formFile" class="form-label mb-3">Foto Produk</label>
                            <br>
                            <img src="{{asset('img/product/'.$p->product_image)}}" width="35%" alt="">
                            <br>
                            <input class="form-control mt-3" type="file" name="product_image" value="{{$p->product_image}}" id="formFile">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center mt-3">
                        <div class="form-group">
                            <a class="btn btn-danger shadow-sm mr-2" href="{{route('product')}}">Batal</a>
                            <button type="submit" class="btn btn-primary shadow-sm">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </div>
</div>