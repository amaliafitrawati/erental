@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/modal.css') }}" />
    </head>
    <body>
        <div class="card">
            <div class="card-header">
                <h4 class="m-0 font-weight-bold text-primary">List Product</h4>
            </div>
            <div class="card-body">
                <a href="/product/add" class="btn btn-primary mt-4 mb-4"><i class="fa fa-plus"></i>Tambah</a>
                <table class="table table-bordered" id="car-table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Foto Produk</th>
                            <th>Tipe</th>
                            <th>Tahun</th>
                            <th>Harga Sewa</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($product as $p)
                        <tr> 
                            <td>{{ $p->name}}</td>
                            <td><img src="{{asset('img/product/'.$p->product_image)}}" width="40%"></td>
                            <td>{{ $p->typename}}</td>
                            <td>{{ $p->year}}</td>
                            <td>{{ $p->price}}</td>
                            <td>
                                <a title="Edit" href="/product/edit/{{ $p->id }}" class="btn btn-primary"><i class='fa fa-pen'></i></a>
                                <a title="Hapus" data={{$p->id}} href="#" class="btn btn-danger delete-button"><i class='fa fa-trash'></i></a>
                            </td>
                        </tr>

                        <div class="overlay-modal">
                            <div class="confirmation-modal">
                                <img class="icon-modal" src="{{ URL::asset('img/alert.png') }}" alt="Alert">

                                <div class="text-modal">
                                    <h2>Konfirmasi</h2>
                                    <p>Anda ingin menghapus</p>
                                </div>

                                
                                <div class="buttons-modal">
                                    <a class="no-button" title="Edit" href="#">
                                        Batal
                                    </a>
                                    <a class="yes-button" title="Hapus" href="/product/delete/">
                                        Hapus
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        

        <script src="{{ URL::asset('js/modalConfirmation.js') }}"></script>
    </body>
</html>



