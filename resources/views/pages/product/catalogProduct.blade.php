@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<h3>Katalog Produk</h3>
<div class="card-deck">
@foreach($product as $p)
  <div class="card stretch-card">
    <img class="card-img-top" style="height:60%!important;" src="{{asset('img/product/'.$p->product_image)}}" alt="Card image cap">
    <div class="card-body">
      <center><h5 class="card-title">{{$p->name}}</h5></center>
      <p class="card-text">
        <small>Jenis Kendaraan : <br>Rp. {{$p->price}}</small></p>
        <small>Plat Nomor: <br>Rp. {{$p->license_number}}</small></p>
        <small>Tahun Kendaraan : <br>Rp. {{$p->year}}</small></p>
        <small>Harga Sewa Per Hari : <br>Rp. {{$p->price}}</small></p>
    </div>
  </div>
  @endforeach
</div>
