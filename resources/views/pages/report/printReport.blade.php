@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
  <div>
      <div class="content-wrapper">
        <div class="row">
          <div class="col-lg-12 stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Cetak Transaksi</h4> <hr>
                  <form action="/report/print">
                    @csrf
                    <div class="form-row">
                      <div class="form-group col-md-5">
                        <label>Tanggal Sewa</label>
                        <input type="date" class="form-control" id="rent_start_date" name="rent_start_date">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-5">
                        <label>s/d</label>
                          <input type="date" class="form-control" id="rent_end_date" name="rent_end_date">
                      </div>
                    </div>
                  <div class="form-row"><button type="submit" class="btn btn-primary">Cetak</button>&nbsp;&nbsp;
                </div>
              </form>
            </div>
            </div>
          </div>

