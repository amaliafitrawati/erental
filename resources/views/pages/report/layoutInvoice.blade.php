<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		.table-bordered{
			font-size: 11pt;
		}
	</style>
	<center>
		<h5>Invoice Transaksi eRental</h4>
	</center>

    <table class="mt-6 mb-3">
        <thead>
            <tr>
                <th>Nama</th>
                <th>:</th>
                <th><?php echo $user?></th>
            </tr>
            <tr>
                <th>Email</th>
                <th>:</th>
                <th><?php echo $userEmail?></th>
            </tr>
        </thead>
    </table>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No Transaksi</th>
				<th>Tanggal Mulai Sewa</th>
				<th>Tanggal Akhir Sewa</th>
                <th>Nama Kendaraan</th>
                <th>Jenis Kendaraan</th>
                <th>Total Harga</th>
			</tr>
		</thead>
		<tbody>
			@foreach($transaction as $t)
			<tr>
				<td>{{$t->transaction_number}}</td>
				<td>{{$t->rent_start_date}}</td>
				<td>{{$t->rent_end_date}}</td>
				<td>{{$t->product_name}}</td>
				<td>{{$t->type_name}}</td>
				<td>Rp. {{$t->total_price}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>
