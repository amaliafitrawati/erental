<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table {
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Transaksi eRental</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>No Transaksi</th>
				<th>Nama Customer</th>
				<th>Tanggal Mulai Sewa</th>
				<th>Tanggal Akhir Sewa</th>
                <th>Nama Kendaraan</th>
                <th>Tipe Kendaraan</th>
                <th>Total Harga</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($transaction as $t)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$t->transaction_number}}</td>
				<td>{{$t->customer_name}}</td>
				<td>{{$t->rent_start_date}}</td>
				<td>{{$t->rent_end_date}}</td>
				<td>{{$t->product_name}}</td>
				<td>{{$t->type_name}}</td>
				<td>Rp. {{$t->total_price}}</td>
				@if($t->status == 0)         
                    <td>Proses Persetujuan</td>  
                @else       
                    <td>Berhasil</td>    
                @endif
			</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>
