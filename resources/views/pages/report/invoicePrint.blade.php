@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
  <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Cetak Invoice</h4> <hr>
                  <form action="/invoice/print">
                      @csrf
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label>Nomor Transaksi</label>
                            <input type="text" name="transaction_number" class="form-control border-dark-50">
                        </div>
                    </div>
                    <div class="form-row"><button type="submit" class="btn btn-primary">Cetak</button>&nbsp;&nbsp;
                  </div>
                  </form>
                </div>
              </div>
            </div>

