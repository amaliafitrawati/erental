@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Form Transaksi</h6>
        </div>
        <div class="card-body">
            <form action="/transaction/makeTransaction" method="POST">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama Customer</label>
                          <input type="text" value="<?php echo $userName?>" class="form-control border-dark-50" disabled="" required="">
                          <input type="hidden" name="user_id" value="<?php echo $userId?>" class="form-control border-dark-50">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                            <div class="form-group">
                            <label>Nama Produk</label>
                            <select name="product_id" id="type" class="form-control">
                                <option value="" disabled>Pilih</option>
                                @foreach ($product as $p)
                                    <option value="{{$p->id}}">{{ $p->name }}</option>
                                @endforeach
                                <input type="hidden" name="product_price" value="{{ $p->price}}">
                            </select>
                            </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Tanggal Sewa</label>
                          <input type="date" name="rent_start_date" class="form-control datepicker" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Tanggal Kembali</label>
                          <input type="date" name="rent_end_date" class="form-control datepicker" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center mt-3">
                        <div class="form-group">
                            <a class="btn btn-danger shadow-sm mr-2" href="{{route('product')}}">Batal</a>
                            <button type="submit" class="btn btn-primary  shadow-sm">Proses</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>