@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
    </head>

<body>
    <div class="card">
        <section class="transaction">
            <div class="card-header py-3">
                <h4 class="m-0 font-weight-bold text-primary">Transaksi Saya</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered">
                        <tr>
                            <th>No Transaksi</th>
                            <th>Tanggal Sewa</th>
                            <th>Tanggal Pengembalian</th>
                            <th>Nama Kendaraan</th>
                            <th>Jenis Kendaraan</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        @foreach($transaction as $t)
                        <tr> 
                            <td>{{ $t->transaction_number}}</td>
                            <td>{{ $t->rent_start_date}}</td>
                            <td>{{ $t->rent_end_date}}</td>
                            <td>{{ $t->product_name}}</td>
                            <td>{{ $t->type_name}}</td>
                            <td>{{ $t->total_price}}</td>
                            @if($t->status == 0)         
                                <td>Proses Persetujuan</td>  
                            @elseif($t->status == 1)         
                                <td>Berhasil</td>  
                            @else
                                <td>Selesai</td>        
                            @endif
                            <td>
                                <a title="Detail" href="/transaction/detail/{{$t->id}}" class="btn btn-warning">Detail</a>
                            </td>
                            @endforeach
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </div>
</body>

</html>