@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi</h6>
        </div>
        <div class="card-body">
            @foreach($transaction as $t)
            <form action="" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $t->id }}">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nomor Transaksi</label>
                          <input type="text" name="transaction_number" id="" value="{{$t->transaction_number }}" class="form-control border-dark-50" required="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama Kendaraan</label>
                          <input type="text" name="product_name" id="" value="{{$t->product_name }}" class="form-control border-dark-50" required="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                            <div class="form-group">
                            <label>Jenis Kendaraan</label>
                            <input type="text" name="type_name" id=""  value="{{$t->type_name }}" class="form-control border-dark-50" required="" readonly="">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Plat Nomor</label>
                          <input type="text" name="license_number" id=""  value="{{$t->license_number }}" class="form-control border-dark-50" required="" readonly="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Tahun</label>
                          <input type="text" name="year" id="" value="{{ $t->year }}" class="form-control border-dark-50" required="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Total Harga</label>
                          <input type="text" name="total_price" id="" value="{{ $t->total_price }}" class="form-control border-dark-50" required="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="formFile" class="form-label mb-3">Foto Produk</label>
                            <br>
                            <img src="{{asset('img/product/'.$t->product_image)}}" width="35%" alt="">
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center mt-3">
                        <div class="form-group">
                            <?php if(Auth::user()->role == 1){ echo '';?>
                                <a class="btn btn-danger shadow-sm mr-2" href="{{route('listTransaction')}}">Kembali</a>
                            <?php }?>
                            <?php if(Auth::user()->role == 2){ echo '';?>
                                <a class="btn btn-danger shadow-sm mr-2" href="{{route('listTransactionUser')}}">Kembali</a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </div>
</div>