@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
        <!-- <link rel="stylesheet" href="{{ URL::asset('css/modal.css') }}" /> -->
    </head>

<body>
    <section class="user">
        <h3>Customer</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Nama</th>
                    <th scope="col">No Telp</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>
                        <button type="button" class="btn btn-primary">
                            <i class='fa fa-pen'></i>
                        </button>
                        <button type="button" class="btn btn-danger">
                            <i class='fa fa-trash'></i>
                        </button>
                    </td>
                </tr>

                
            </tbody>
        </table>
    </section>

    <!-- <script src="{{ URL::asset('js/modalConfirmation.js') }}"></script> -->
</body>

</html>