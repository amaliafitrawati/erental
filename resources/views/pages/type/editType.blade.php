@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
    </head>

    <div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Tipe Produk</h6>
        </div>
        <div class="card-body">
            @foreach($type as $t)
            <form action="/productType/update" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="id" value="{{ $t->id }}"> <br/>

                <div class="form-group">
                    Nama 
                    <input type="text" class="form-control" required="required" name="name" value="{{ $t->name }}"> <br/>
                </div>

                <div class="btnAll">
                    <a href='/productType/list'> <button class="btn btn-danger" type='button' id="formBtnCancel"> Batal </button> 
                    <input class="btn btn-primary" type="submit" value="Submit" name="submit" id="formBtnSubmit">
                </div>

            </form>
            @endforeach
</div>
</div>
    </body>
</html>