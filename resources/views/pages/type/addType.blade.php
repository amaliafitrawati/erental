@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
</head>
<body>
    <div class="card">
        <div class="card-header">
            <h4 class="m-0 font-weight-bold text-primary">Form Tipe Produk</h4>
        </div>
        <div class="card-body">
            <br>
            <form action="/productType/save" method="POST">
                {{ csrf_field() }}
                
                <div class="form-group">
                    Nama 
                    <input type="text" class="form-control" name="name" required="required"> <br/>
                </div>
                
                <div>
                    <a href='/productType/list'> <button class="btn btn-danger" type="button" id="formBtnCancel"> Batal </button> 
                    <input class="btn btn-warning" type="reset" value="Reset" name="reset" id="formBtnReset">
                    <input class="btn btn-primary" type="submit" value="Submit" name="submit" id="formBtnSubmit">
                </div>
            </form>
        </div>
</body>
</html>