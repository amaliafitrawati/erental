@extends('templates.sidebar')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('css/modal.css') }}" />
    </head>

<body>
    <div class="overlay-modal">
        <div class="confirmation-modal">
            <img class="icon-modal" src="{{ URL::asset('img/alert.png') }}" alt="Alert">

            <div class="text-modal">
                <h2>Konfirmasi</h2>
                <p>Anda ingin menghapus</p>
            </div>

                                
            <div class="buttons-modal">
                <a class="no-button" title="Edit" href="#">
                    Batal
                </a>
                <a class="yes-button" title="Delete" href="/user/delete/">
                    Hapus
                </a>
            </div>
        </div>
    </div>
    <div class="card">
        <section class="user">
            <div class="card-header py-3">
                <h4 class="m-0 font-weight-bold text-primary">Daftar User</h4>
            </div>
            <div class="card-body">
                <a href="/user/add" class="btn btn-primary mt-4 mb-4"><i class="fa fa-plus"></i>Tambah</a>
                <div class="table-responsive">
                    <table class="table table-sm table-bordered">
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                    @foreach($user as $u)
                    <tr>
                        <td>{{ $u->name }}</td>
                        <td>{{ $u->email }}</td>
                        @if($u->role == 1)         
                            <td>Administrator</td>         
                        @else
                            <td>User</td>        
                        @endif
                        <td>
                            <a title="Edit" href="/user/edit/{{ $u->id }}" class="btn btn-primary"><i class='fa fa-pen'></i></a>
                            <a title="Delete" data={{ $u->id }} href="#" class="btn btn-danger delete-button"><i class='fa fa-trash delete-button'></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </section>
    </div>
    <script src="{{ URL::asset('js/modalConfirmation.js') }}"></script>
</body>

</html>