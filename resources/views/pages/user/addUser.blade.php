@extends('templates.sidebar')
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tambah User</h6>
        </div>
        <div class="card-body">
            <form action="/user/save" method="POST">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama</label>
                          <input type="text" name="name" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" name="email" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" name="password" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                            <div class="form-group">
                            <label>Role</label>
                            <select name="role" id="type" class="form-control">
                                <option value="" disabled>Pilih</option>
                                    <option value="1">Admin</option>
                                    <option value="2">User</option>
                            </select>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center mt-3">
                        <div class="form-group">
                            <a class="btn btn-danger shadow-sm mr-2" href="{{route('user')}}">Batal</a>
                            <button type="submit" class="btn btn-primary  shadow-sm">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>