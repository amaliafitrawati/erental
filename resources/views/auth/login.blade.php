<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Masuk</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/auth.css') }}" />
    </head>
    <body class="position-relative">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <a href="/" class="navbar-brand">Rental.com</a>
                <div class="d-flex jus">
                    <a href="/login">
                        <button class="btn btn-primary mr-4" type="submit">Masuk</button>
                    </a>
                    <a href="/register">
                        <button type="button" class="btn btn-outline-primary">Daftar</button>
                    </a>
                </div>
            </div>
        </nav>
        <div class="form-container">
            <form action="/actionlogin" method="POST" class="form-style" >
                <h2>Masuk</h2>
                @if(session('error'))
                <div class="alert alert-danger">
                   {{session('error')}}
                </div>
                @endif
                <form action="{{ route('actionlogin') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>
                <button type="submit" class="btn btn-primary mt-3">Masuk</button>
                <div class="mt-4">
                    <center>
                        <p>Belum Punya Akun?</p>
                        <p><a href="/register">Daftar Akun</a></p>
                    </center>
                </div>
            </form>
        </div>
    </body>
</html>