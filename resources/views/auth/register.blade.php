<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daftar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <link rel="stylesheet" href="{{ URL::asset('css/auth.css') }}" />
    </head>
    <body class="position-relative">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <a href="/" class="navbar-brand">Rental.com</a>
                <div class="d-flex jus">
                    <a href="/login">
                        <button class="btn btn-primary mr-4" type="submit">Masuk</button>
                    </a>
                    <a href="/register">
                        <button type="button" class="btn btn-outline-primary">Daftar</button>
                    </a>
                </div>
            </div>
        </nav>
        <div class="form-container">
            <form class="form-style" action="/createAcc" method="POST">
                <h2>Registrasi</h2>
                {{ csrf_field() }}
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" id="email">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Daftar</button>
            </form>
        </div>
    </body>
</html>