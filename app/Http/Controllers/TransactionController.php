<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\CarbonPeriod;

class TransactionController extends Controller{

    public function listTransaction(){
        $transaction = DB::table('transaction')
        ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
        ->leftJoin('types', 'product.type_id', '=', 'types.id')
        ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
        ->select('transaction.*', 'types.name as type_name', 'product.name as product_name', 
                   'users.name as customer_name')
        ->get();

        return view('pages.transaction.listTransaction',['transaction' => $transaction]);
    }

    public function listTransactionUser(){
        $userId = Auth::user()->id;
        $transaction = DB::table('transaction')
        ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
        ->leftJoin('types', 'product.type_id', '=', 'types.id')
        ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
        ->select('transaction.*', 'types.name as type_name', 'product.name as product_name', 
                   'users.name as customer_name')
        ->where('transaction.user_id', '=', $userId)
        ->get();

        return view('pages.transaction.listTransactionUser',['transaction' => $transaction]);
    }

    public function formTransaction(){
        $userName = Auth::user()->name;
        $userId = Auth::user()->id;
        $product = DB::table('product')->get();
        return view('pages.transaction.formTransaction' , ['userName' => $userName, 'userId' => $userId, 'product' => $product]);
    }

    public function makeTransaction(Request $request){
        $transactionNumber = str_replace(".","",microtime(true)).rand(000,99);

        $period = CarbonPeriod::create($request->rent_start_date, $request->rent_end_date);
        $count = $period->count();  

        $productPrice = DB::table('product')
        ->select('product.price')
        ->where('id', '=', $request->product_id)
        ->get();

        $totalPrice = $count * $productPrice[0]->price;

        DB::table('transaction')->insert([
            'user_id' => $request->user_id,
            'product_id' => $request->product_id,
            'rent_start_date' => $request->rent_start_date,
            'rent_end_date' => $request->rent_end_date,
            'total_price' => $totalPrice,
            'status' => 0,
            'transaction_number' => $transactionNumber
        ]);
        return redirect('/transaction/user/list');
    }

    public function detail($id){
        $transaction = DB::table('transaction')
        ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
        ->leftJoin('types', 'product.type_id', '=', 'types.id')
        ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
        ->select('transaction.*', 'types.name as type_name', 'product.name as product_name', 
                   'users.name as customer_name', 'product.license_number as license_number',
                   'product.year as year', 'product.product_image as product_image')
        ->where('transaction.id',$id)
        ->get();
        return view('pages.transaction.detailTransaction',['transaction' => $transaction]);
    }

    public function approve($id){
        DB::table('transaction')->where('id',$id)->update([
            'status' => 1
        ]);
        return redirect('/transaction/list');
    }
    
}