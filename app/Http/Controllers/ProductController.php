<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\Product;

class ProductController extends Controller{
    
    public function listProduct(){
        $product = DB::table('product')
                    ->leftJoin('types', 'product.type_id', '=', 'types.id')
                    ->select('product.*', 'types.name as typename')
                    ->get();
    	return view('pages.product.listProduct',['product' => $product]);
    }

    public function add(){
        $type = Type::pluck('name', 'id');
        return view('pages.product.addProduct', ['type' => $type]);
    }

    public function save(Request $request){
        $image = $request->product_image;
        $imageName = $image->getClientOriginalName();
        DB::table('product')->insert([
            'id' => $request->id,
            'name' => $request->name,
            'type_id' => $request->type_id,
            'product_image' => $imageName,
            'license_number' => $request->license_number,
            'year' => $request->year,
            'price' => $request->price
        ]);
        $image->move(public_path().'/img/product', $imageName);
        
        return redirect('/product/list');
    }

    public function edit($id){
        $product = DB::table('product')->where('id',$id)->get();
        $type = Type::pluck('name', 'id');
        return view('pages.product.editProduct', ['product' => $product, 'type' => $type]);
    }

    public function update(Request $request){
        if($request->has('product_image')){
            $image = $request->has($request->product_image);
            $imageName = $image->getClientOriginalName();
            $image->move(public_path().'/img/product', $imageName);
            DB::table('product')->where('id',$request->id)->update([
                'name' => $request->name,
                'type_id' => $request->type_id,
                'product_image' => $imageName,
                'license_number' => $request->license_number,
                'year' => $request->year,
                'price' => $request->price
            ]);
        }else{
            DB::table('product')->where('id',$request->id)->update([
                'name' => $request->name,
                'type_id' => $request->type_id,
                'license_number' => $request->license_number,
                'year' => $request->year,
                'price' => $request->price
            ]);
        }        
        
        return redirect('/product/list');
    }

    public function delete($id){
        DB::table('product')->where('id',$id)->delete();
        return redirect('/product/list');
    }

    public function catalog(){
        $product = DB::table('product')
                    ->leftJoin('types', 'product.type_id', '=', 'types.id')
                    ->select('product.*', 'types.name as typename')
                    ->get();
    	return view('pages.product.catalogProduct',['product' => $product]);
    }
}
