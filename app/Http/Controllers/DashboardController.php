<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller{

    public function dashboard(){
        $user = Auth::user()->name;
        return view('templates.dashboard',['user' => $user]);
    }
    
}
