<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller{

    public function listCustomer(){
        return view('pages.customer.listCustomer');
    }

    public function listUser(){
        $user = DB::table('users')->get();
    	return view('pages.user.listUser',['user' => $user]);
    }

    public function delete($id){
        DB::table('users')->where('id',$id)->delete();
        return redirect('/user/list');
    }

    public function edit($id){
        $user = DB::table('users')->where('id',$id)->get();
        return view('pages.user.editUser',['user' => $user]);
    }

    public function add(){
        return view('pages.user.addUser');
    }

    public function save(Request $request){
        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);
        return redirect('/user/list');
    }

    public function update(Request $request){
        DB::table('users')->where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);
        return redirect('/user/list');
    }

}
