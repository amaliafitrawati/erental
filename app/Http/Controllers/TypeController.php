<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TypeController extends Controller{

    public function list(){
        $productType = DB::table('types')->get();
    	return view('pages.type.listType',['productType' => $productType]);
    }

    public function add(){
        return view('pages.type.addType');
    }

    public function save(Request $request){
        DB::table('types')->insert([
            'name' => $request->name
        ]);
        return redirect('/productType/list');
    }

    public function edit($id){
        $type = DB::table('types')->where('id',$id)->get();
        return view('pages.type.editType',['type' => $type]);
    }

    public function update(Request $request){
        DB::table('types')->where('id',$request->id)->update([
            'name' => $request->name
        ]);
        return redirect('/productType/list');
    }


    public function delete($id){
        DB::table('types')->where('id',$id)->delete();
        return redirect('/productType/list');
    }

    

}
