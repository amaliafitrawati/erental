<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller{

    public function index(){
        return view('templates.index');
    }

    public function login(){
        if (Auth::check()){
            return redirect('dashboard');
        }else{
            return view('auth.login');
        }
    }

    public function actionlogin(Request $request)
    {
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (Auth::Attempt($data)) {
            return redirect('dashboard');
        }else{
            Session::flash('error', 'Email atau Password Salah');
            return redirect('/login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function regist(){
        return view('auth.register');
    }

    public function createAcc(Request $request){
        \DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 2
        ]);
        echo "<script>";
        echo "alert('Selamat akun anda berhasil terdaftar!');";
        echo "</script>";
        return view('auth.login');

    }
}
