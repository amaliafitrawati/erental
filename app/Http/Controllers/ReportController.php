<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PDF;

class ReportController extends Controller{

    public function index() {
        return view('pages.report.printReport');
    }

    public function print(Request $request){
    	$transaction = DB::table('transaction')
                        ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                        ->leftJoin('types', 'product.type_id', '=', 'types.id')
                        ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                        ->select('transaction.*', 'types.name as type_name', 'product.name as product_name', 
                                'users.name as customer_name')
                        ->whereBetween('rent_start_date', [$request->rent_start_date, $request->rent_end_date])
                        ->get();
    	$pdf = PDF::loadview('pages.report.layoutReport',['transaction'=>$transaction]);
    	return $pdf->download('laporan-transaksi-erental');
    }

    public function invoice() {
        return view('pages.report.invoicePrint');
    }

    public function invoicePrint(Request $request){
        $user = Auth::user()->name;
        $userEmail = Auth::user()->email;
    	$transaction = DB::table('transaction')
                        ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                        ->leftJoin('types', 'product.type_id', '=', 'types.id')
                        ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                        ->select('transaction.*', 'types.name as type_name', 'product.name as product_name', 
                                'users.name as customer_name')
                        ->where('transaction_number' , '=', $request->transaction_number)
                        ->get();
 
    	$pdf = PDF::loadview('pages.report.layoutInvoice',['transaction'=>$transaction, 
                'user' => $user, 'userEmail' => $userEmail]);
    	return $pdf->download('invoice-erental');
    }
}
