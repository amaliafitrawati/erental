<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;

Route::get('/', 'App\Http\Controllers\LoginController@index');
Route::get('/login', 'App\Http\Controllers\LoginController@login')->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');
Route::get('/register', 'App\Http\Controllers\LoginController@regist');
Route::post('/createAcc','App\Http\Controllers\LoginController@createAcc')->name('createAcc');
Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::post('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');



Route::middleware(["web","auth"])->group(function(){
    //Dashboard 
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@dashboard')->name('dashboard');

    //User
    Route::get('/user/list', 'App\Http\Controllers\UserController@listUser')->name('user');
    Route::get('/user/delete/{id}','App\Http\Controllers\UserController@delete');
    Route::get('/user/edit/{id}','App\Http\Controllers\UserController@edit');
    Route::get('/user/add','App\Http\Controllers\UserController@add');
    Route::post('/user/save','App\Http\Controllers\UserController@save');
    Route::post('/user/update','App\Http\Controllers\UserController@update');

    //Product
    Route::get('/product/list', 'App\Http\Controllers\ProductController@listProduct')->name('product');
    Route::get('/product/add','App\Http\Controllers\ProductController@add');
    Route::post('/product/save','App\Http\Controllers\ProductController@save');
    Route::get('/product/edit/{id}','App\Http\Controllers\ProductController@edit');
    Route::post('/product/update','App\Http\Controllers\ProductController@update');
    Route::get('/product/delete/{id}','App\Http\Controllers\ProductController@delete');
    Route::get('/product/catalog', 'App\Http\Controllers\ProductController@catalog')->name('catalog');

    //Product Type
    Route::get('/productType/list', 'App\Http\Controllers\TypeController@list')->name('listProductType');
    Route::get('/productType/add','App\Http\Controllers\TypeController@add');
    Route::post('/productType/save','App\Http\Controllers\TypeController@save');
    Route::get('/productType/edit/{id}','App\Http\Controllers\TypeController@edit');
    Route::post('/productType/update','App\Http\Controllers\TypeController@update');
    Route::get('/productType/delete/{id}','App\Http\Controllers\TypeController@delete');

    //Transaction
    Route::get('/transaction/list', 'App\Http\Controllers\TransactionController@listTransaction')->name('listTransaction');
    Route::get('/transaction/form', 'App\Http\Controllers\TransactionController@formTransaction')->name('formTransaction');
    Route::post('/transaction/makeTransaction', 'App\Http\Controllers\TransactionController@makeTransaction')->name('makeTransaction');
    Route::get('/transaction/print', 'App\Http\Controllers\TransactionController@formTransaction')->name('printTransaction');
    Route::get('/transaction/approve/{id}','App\Http\Controllers\TransactionController@approve');
    Route::get('/transaction/detail/{id}','App\Http\Controllers\TransactionController@detail');
    Route::get('/transaction/user/list', 'App\Http\Controllers\TransactionController@listTransactionUser')->name('listTransactionUser');

    //Report
    Route::get('/report', 'App\Http\Controllers\ReportController@index')->name('report');
    Route::get('/report/print', 'App\Http\Controllers\ReportController@print')->name('print');
    Route::get('/invoice', 'App\Http\Controllers\ReportController@invoice')->name('invoice');
    Route::get('/invoice/print', 'App\Http\Controllers\ReportController@invoicePrint')->name('invoicePrint');
});
